"use strict";

const express = require('express');
const mapsRunner = require('./maps_runner');

const app = express();
app.use(express.static(__dirname + './../../'));

const chart = require('./chart');

function init() {
  app.get('/', (req, res) => res.send('hello world'));
  app.get('/current', handleCurrentPlainRequest);
  app.get('/current.json', handleCurrentJsonRequest);

  app.listen(7000, () => console.log('Started listening on 7000'));
}

async function getData() {
  let data = await mapsRunner.getTravelTime();
  let result = {};

  if (data && data.elements) {
    // Parseble data found
    let way = data.elements.find(elem => elem.distance.value < 30500);

    if (way) {
      // Found the regular way to work.
      result = way;
    }
    else {
      // The regular way is closed
      result = {
        status: 'ERR',
        message: 'Could not use the short way.'
      };
    }
  }
  else {
    // API response does not contain information
    result = {
      status: 'ERR',
      data: data
    }
  }

  return result;
}

async function handleCurrentPlainRequest(req, res) {
  let data = await getData();

  let { duration_in_traffic } = data;

  let result = 'No result found';

  if (duration_in_traffic && duration_in_traffic.text) {
    let plottedData = await chart.draw();

    result = `<p>Your current travel time is ${duration_in_traffic.text}.</p><pre>${plottedData}</pre>`;
  }

  res.status(200);
  res.send(result);
}

async function handleCurrentJsonRequest(req, res) {
  let data = await getData();

  res.status(200);
  res.json(data);
}

module.exports = {
  init
}
