"use strict";

const config = require('config');

let key = config.get('google.maps.api_key');

console.log(`Google API key: ${key}`);

if (!key) throw('Unable to read Google API key.')

const googleMapsClient = require('@google/maps').createClient({
  key: key,
  Promise: Promise
});

async function getTravelTime () {
  let result = null;

  try {
    result = await googleMapsClient.distanceMatrix({
      destinations: [
        config.get('google.maps.destination')
      ],
      origins: [
        config.get('google.maps.origin')
      ],
      mode: 'driving',
      departure_time: 'now',
      traffic_model: 'best_guess'
    }).asPromise()

    if (result && result.json) {
      result = result.json.rows[0];
    }
    
  } catch (e) {
    /* handle error */
    console.log(e);
  }

  return result;
}

module.exports = {
  getTravelTime: getTravelTime
};

