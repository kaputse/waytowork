"use strict";

const web = require('./web');
const cron = require('./background_worker');

web.init();
cron.init();

