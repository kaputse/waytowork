"use strict";

const asciichart = require('asciichart');
const clear      = require('clear');
const fs         = require('fs');
const util       = require('./util');

async function processData() {
  let data = null;

  try {
    data = await new Promise((resolve, reject) => {
      fs.readFile(`${util.getDate()}.log`, 'utf8', (err, d) => {
        if (err) {
          console.error('ERR:- ' + err);
          reject(err);
        }

        resolve(d.toString().trim());
      });
    });
  } catch (e) {
    throw(e);
  }

  data = data
    .split("\n")
    .map(d => d.split(",")[1])
    .map(x => parseInt(x, 10));

  return data; 
}

module.exports.draw = async function () {
  let data = [];

  try {
    data = await processData();
  } catch (e) {
    throw(e);
  }

  return new Promise((resolve, reject) => {
    let out = asciichart.plot (data);
    resolve(out);
  })
}
