"use strict";

module.exports.getDate = function () {
  let d = new Date();
  return `${d.getFullYear()}-${d.getMonth()}-${d.getDate()}`;
}
