"use strict";

const fs = require('fs');
const mapsRunner = require('./maps_runner');
const chart = require('./chart');
const util = require('./util');

function logToFile (durationInTraffic) {
  let fileName = `${util.getDate()}.log`;

  let logData = () => {
    fs.appendFile(
      fileName,
      `${Date.now()},${Math.ceil(durationInTraffic.value/60)}\n`,
      (err) => {
        if (err) throw err;
      }
    );
  }

  fs.exists(fileName, (exists) => {
    if (exists == false) {
      // Initialize new daily log file.
      fs.appendFile(
        fileName,
        `${Date.now()},0\n`,
        (err) => {
          if (err) throw err;

          logData();
        }
      );
    } else {
        logData();
    }

  });
}

async function run (timeout) {
  let data = await mapsRunner.getTravelTime();
  
  if (data && data.elements) {
    // Parseble data found
    let way = data.elements.find(elem => elem.distance.value < 30500);

    if (way) {
      // Found the regular way to work.
      let { duration_in_traffic } = way;

      if (duration_in_traffic && duration_in_traffic.text) {

        logToFile(duration_in_traffic);

        console.info(`OK: Your current travel time is ${duration_in_traffic.text}.`);
      }
    }
    else {
      // The regular way is closed
      console.error('ERR: Could not use the short way.');
    };
  }
  else {
    // API response does not contain information
    console.error('ERR: API error.');
  }

  setTimeout(run, timeout, timeout);
}

module.exports.init = function (minutes = 5) {
  console.log(`Starting background processing every ${minutes} mins ...`);

  return run(minutes * 60 * 1000);
}
